# модуль, реализующий функциональность игры.
# Функции реализаци игры:
#   загадать_число()
#   проверить_число(NN) - возвращает словарь {'bulls': N, 'cows': N}
# Загаданное число хранить в глобальной переменной
# Обратите внимание, что строки - это список символов
#
import random
user_number = []

def get_user_number():
    user_input = input("Введите четырехзначное число: ")
    user_number = list(map(int, user_input))

def get_secret_number():
    first_digit = random.sample([1, 2, 3, 4, 5, 6, 7, 8, 9], 1)
    start_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    start_list.remove(first_digit[0])
    over_digits = random.sample(start_list, 3)
    secret_number = first_digit + over_digits

def chek_numbes():
    cow = 0
    bull = 0
    count = 1
    while True:
        for element in range(len(user_number)):
            if user_number[element] == secret_number[element]:
                bull += 1
        for element in range(len(user_number)):
            for num in range(len(secret_number)):
                if user_number[element] == secret_number[num]:
                    cow += 1
        cow -= bull
        cowbull = {'Быки': bull, 'Коровы': cow}
        print(cowbull)
        if bull == 4:
            print("Быков 4 Вы выиграли! Попыток:", count)
            break
        cow = 0
        bull = 0
        count += 1
    choice = int(input("Еще раз? 1-да, 0-нет"))
    if choice == 1:
        chek_numbes()
    else:
        print("Хорошо поиграли! До новых встреч:)")



