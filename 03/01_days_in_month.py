# -*- coding: utf-8 -*-

# (if/elif/else)

# По номеру месяца вывести кол-во дней в нем
# Результат проверки вывести на консоль
# Если номер месяца некорректен - сообщить об этом

# Номер месяца получать от пользователя следующим образом
user_input = input("Введите, пожалуйста, номер месяца: ")
month = int(user_input)
print('Вы ввели', month)

# TODO - Можно было так: elif month in [1, 3, 5, 7, 10, 12]:
if month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12:
    print('В указанном месяце 31 день')
elif month == 4 or month == 6 or month == 9 or month == 11:
    print('В указанном месяце 30 дней')
elif month == 2:
    print('В указанном месяце 28 или 29 дней')
else:
    print('Вы ввели некорректный номер месяца')

# зачёт!
