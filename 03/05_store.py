# -*- coding: utf-8 -*-

# Есть словарь кодов товаров

goods = {
    'Лампа': '12345',
    'Стол': '23456',
    'Диван': '34567',
    'Стул': '45678',
}

# Есть словарь списков количества товаров на складе.

store = {
    '12345': [
        {'quantity': 27, 'price': 42},
    ],
    '23456': [
        {'quantity': 22, 'price': 510},
        {'quantity': 32, 'price': 520},
    ],
    '34567': [
        {'quantity': 2, 'price': 1200},
        {'quantity': 1, 'price': 1150},
    ],
    '45678': [
        {'quantity': 50, 'price': 100},
        {'quantity': 12, 'price': 95},
        {'quantity': 43, 'price': 97},
    ],
}


for name, code in goods.items():
    index = 0
    cost = 0
    quantity = 0
    for item in store[code]:
        # TODO - Вы перебираете элементы списка. На каждой итерации элемент теущий элемент списка - в item
        #  Перепишите код, используя item в этом цикле и попутно избавьтесь от переменной index
        cost += store[code][index]['quantity'] * store[code][index]['price']
        quantity += store[code][index]['quantity']
        index += 1
    print(name, '-', quantity, 'шт, стоимость', cost, 'руб')






