# Евгений проходит специальный тест по программированию. У него всё шло хорошо, пока он не наткнулся на тему «Функции».
# Задание звучит так:
#
# Основная ветка программы, не считая заголовков функций, состоит из одной строки кода. Это вызов функции test().
# В ней запрашивается на ввод целое число. Если оно положительное, то вызывается функция positive(),
# тело которой содержит команду вывода на экран слова «Положительное». Если число отрицательное,
# то вызывается функция negative(), её тело содержит выражение вывода на экран слова «Отрицательное».
#
# Помогите Евгению и реализуйте такую программу.

def positive():
    print('Положительное')


def negative():
    print('Отрицательное')


def test():
    user_number = int(input('Введите целое число: '))
    # TODO - Неудачная запись. Обычно тернарный оператор используют для получения результата
    #  и сохранения в переменную сразу. Это не ошибка, но так не пишут
    positive() if user_number > 0 else negative()


test()

# зачёт!
