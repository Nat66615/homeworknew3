# -*- coding: utf-8 -*-

# Вывести на консоль жителей комнат (модули room_1 и room_2)
# Формат: В комнате room_1 живут: ...

from room_1 import folks
from room_2 import folks as folk

print("В комнате room_1 живут:", folks[0], "и", folks[1])
print("В комнате room_2 живёт:", folk[0])
