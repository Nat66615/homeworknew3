# -*- coding: utf-8 -*-

# Составить список всех живущих на районе и Вывести на консоль через запятую
# Формат вывода: На районе живут ...
# подсказка: для вывода элементов списка через запятую можно использовать функцию строки .join()
# https://docs.python.org/3/library/stdtypes.html#str.join

from district.central_street.house1.room1 import folks as folks_cs11
from district.central_street.house1.room2 import folks as folks_cs12
from district.central_street.house2.room1 import folks as folks_cs21
from district.central_street.house2.room2 import folks as folks_cs22
from district.soviet_street.house1.room1 import folks as folks_ss11
from district.soviet_street.house1.room2 import folks as folks_ss12
from district.soviet_street.house2.room1 import folks as folks_ss21
from district.soviet_street.house2.room2 import folks as folks_ss22

area_residents = folks_cs11
print("На районе живут: ", (', '.join(folks_cs11)), ",", (', '.join(folks_cs12)), ",", (', '.join(folks_cs21)), ",",
      (', '.join(folks_cs22)), ",", (', '.join(folks_ss11)), ",", (', '.join(folks_ss12)), ",",
      (', '.join(folks_ss21)), ",", (', '.join(folks_ss22)))
