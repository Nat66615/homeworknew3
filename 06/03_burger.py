# -*- coding: utf-8 -*-

# Создать модуль my_burger. В нем определить функции добавления инградиентов:
#  - булочки
#  - котлеты
#  - огурчика
#  - помидорчика
#  - майонеза
#  - сыра
# В каждой функции выводить на консоль что-то вроде "А теперь добавим ..."

# В этом модуле создать рецепт двойного чизбургера (https://goo.gl/zA3goZ)
# с помощью фукций из my_burger и вывести на консоль.

# Создать рецепт своего бургера, по вашему вкусу.
# Если не хватает инградиентов - создать соответствующие функции в модуле my_burger

from my_burger import *

print("Двойной чизбургер:")
add_bun()
add_sause()
add_salad()
add_cutlet()
add_cheese()
add_cutlet()
add_cheese()
add_tomato()
add_onion()
add_sause()
add_bun()

print("мой Бургер:")
add_bun()
add_sause()
add_salad()
add_cheese()
add_cutlet()
add_onion()
add_cheese()
add_tomato()
add_cucumber()
add_sause()
add_bun()
