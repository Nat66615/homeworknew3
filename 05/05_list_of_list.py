# Дан такой список:
#
nice_list = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10]], [[11, 12, 13], [14, 15], [16, 17, 18]]]


#
# Напишите рекурсивную функцию, которая раскрывает все вложенные списки, то есть оставляет только внешний список.
#
# Ответ: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]

def open_list(nice_list):
    if nice_list == []:
        return nice_list
    if isinstance(nice_list[0], list):
        return (open_list(nice_list[0]) + open_list(nice_list[1:]))
    return (nice_list[:1] + open_list(nice_list[1:]))


print('Ответ: ', open_list(nice_list))
open_list(nice_list)

# зачёт!
