# Как вы знаете, в Python есть полезная функция sum, которая умеет находить сумму элементов списков.
# Но иногда базовых возможностей функций не хватает для работы и приходится их усовершенствовать.
#
# Напишите свою функцию sum, которая должна быть более гибкой, чем стандартная функция sum. Она должна уметь:
#
# складывать числа из списка списков;
# складывать числа из набора параметров.
# Основной код оставьте пустым или закомментированным (используйте его только для тестирования).
#
# Примеры вызовов функции:
#
# sum([[1, 2, [3]], [1], 3])
# Ответ в консоли: 10
#
# sum(1, 2, 3, 4, 5)
# Ответ в консоли: 15


def super_sum(sp):
    # sp = [[1, 2, [3]], [1], 3]# вставить объект для рассчета суммы
    if type(sp) == tuple:
        print(sum(sp))
    elif type(sp) == list:
        sp_aligne = []
        while sp:
            e = sp.pop()
            if type(e) == list:
                sp.extend(e)
            else:
                sp_aligne.append(e)
        print(sum(sp_aligne))


super_sum([[1, 2, [3]], [1], 3, [1, 2, [3, [5, 1]]]])
# зачёт!
